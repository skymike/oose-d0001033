import java.awt.GridLayout;
import java.awt.Image;
import java.util.ArrayList;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class GomokuChessBoard extends ChessBoard{

	private final int NULL_PIECE = 0;
	private final int BLACK_PIECE = 1;
	private final int WHITE_PIECE = 2;
	private ArrayList<GomokuChess> pieceList;
	
	public GomokuChessBoard(){
		
	
		super.board = new JLabel();
		super.displayPlayerTurn = new JLabel("Player1");
		super.displayWinner = new JLabel("Ongoing...");
		super.boardImage = new ImageIcon("GomokuBoard.jpg");
		this.pieceList = new ArrayList<GomokuChess>();

		super.Xsize = 15;
		super.Ysize = 15;
		
		chessOnBoard = new int[Xsize][Ysize];
		board.setLayout(new GridLayout(Xsize,Ysize));
		board.setIcon(boardImage);
		     
		
	}
	
	
	private void setChess(int x, int y){
		
		GomokuChess piece = new GomokuChess(x,y);
		piece.setSize(30, 30);
		piece.addMouseListener(ruleOfMoving);
		board.add(piece);
		pieceList.add(piece);
	}
	
	public void newBoard(){
		int x,y;
		x = 0;
		y = 0;
		while(y < Ysize){
			x = 0;
			while(x < Xsize){
				this.setChess(x,y);
				x = x + 1;
			}
			y = y + 1;
		}
	}


	public boolean putChess(GomokuChess piece, String color) {
		
		GomokuChess p = (GomokuChess) piece;
		GomokuRuleOfVictory grov = (GomokuRuleOfVictory) super.getRuleOfVictory();

		ImageIcon Black = new ImageIcon("�¤l.png");
		ImageIcon White = new ImageIcon("�դl.png");
		Black.setImage(Black.getImage().getScaledInstance(Black.getIconWidth()-70,Black.getIconHeight()-70
				   ,Image.SCALE_DEFAULT));	
		White.setImage(White.getImage().getScaledInstance(White.getIconWidth()-70,White.getIconHeight()-70
				   ,Image.SCALE_DEFAULT));	
		int XPosition;
		int YPosition;
		boolean blackVictoryFlag = false;
		boolean whiteVictoryFlag = false;
		
		XPosition = p.getXposition();
		YPosition = p.getYposition();
		
		if(chessOnBoard[XPosition][YPosition] == NULL_PIECE){
			
			p.setColor(color);
			
			if(color.equals("Black")){
				p.setPieceImage(Black);
				chessOnBoard[XPosition][YPosition] = BLACK_PIECE;
				blackVictoryFlag = grov.adjudgeVictory(XPosition, YPosition, BLACK_PIECE);
				this.displayPlayerTurn.setText("Player2");
			}else {
				p.setPieceImage(White);
				chessOnBoard[XPosition][YPosition] = WHITE_PIECE;
				whiteVictoryFlag = grov.adjudgeVictory(XPosition, YPosition, WHITE_PIECE);
				this.displayPlayerTurn.setText("Player1");
			}
			
			if(blackVictoryFlag == true){
				this.terminateGame();
				this.displayWinner.setText("Player1 win the Game!");

				
			}else if(whiteVictoryFlag == true){
				this.terminateGame();
				this.displayWinner.setText("Player2 win the Game!");

			}else{
				System.out.println("Game continue...");
			}
			
			return true;
					
		}else{
			
			return false;
		}
		
		
	}


	@Override
	public void terminateGame() {
		
		int i = 0;
		while(i < this.pieceList.size()){
			pieceList.get(i).removeMouseListener(ruleOfMoving);;
			i = i + 1;
		}
		
	}
		
	
}
