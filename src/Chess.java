import javax.swing.ImageIcon;
import javax.swing.JLabel;


public abstract class Chess extends JLabel{

	
	protected String color;
	
	protected int Xposition;
	
	protected int Yposition;
	
	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getXposition() {
		return Xposition;
	}

	public void setXposition(int xposition) {
		Xposition = xposition;
	}

	public int getYposition() {
		return Yposition;
	}

	public void setYposition(int yposition) {
		Yposition = yposition;
	}
}
