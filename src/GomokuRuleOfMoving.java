import java.awt.event.MouseEvent;

import javax.swing.JLabel;


public class GomokuRuleOfMoving extends RuleOfMoving{
	
	private boolean movingFlag;
	private GomokuChessBoard board;

	public GomokuRuleOfMoving(GomokuChessBoard b){

		this.board = b;
		super.playerFlag = PLAYER1;
	}

	@Override
	public void mouseClicked(MouseEvent event) {
		
		
		if(event.getButton() == MouseEvent.BUTTON1){
			
			GomokuChess piece = (GomokuChess)event.getComponent();
			
			if(playerFlag == PLAYER1){
				movingFlag = board.putChess(piece, "Black");
			}else if(playerFlag == PLAYER2){
				movingFlag = board.putChess(piece, "White");
			}
			
			if(movingFlag == true){
				super.changePlayerFlag();
			}		
		}
	}



}
