import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.ImageIcon;
import javax.swing.JLabel;


public class TaiwanChessBoard extends ChessBoard{
	
	private final int R_COMMANDER = 14;
	private final int R_KNIGHT = 13;
	private final int R_DEPUTY = 12;
	private final int R_CHARIOT = 11;
	private final int R_HORSE= 10;
	private final int R_GUN = 9;
	private final int R_SOLDIER = 8;
	private final int B_COMMANDER = 7;
	private final int B_KNIGHT = 6;
	private final int B_DEPUTY = 5;
	private final int B_CHARIOT = 4;
	private final int B_HORSE= 3;
	private final int B_GUN = 2;
	private final int B_SOLDIER = 1;
	
	private final int COMMANDER = 7;
	private final int KNIGHT = 6;
	private final int DEPUTY = 5;
	private final int CHARIOT = 4;
	private final int HORSE= 3;
	private final int GUN = 2;
	private final int SOLDIER = 1;
	
	protected final int PLAYER1 = 1;
	protected final int PLAYER2 = 2;

	private ArrayList<TaiwanChess> pieceList;
	private JLabel displayPlayerHoldChess;

	
	public TaiwanChessBoard(){
		
		super.board = new JLabel();
		super.displayPlayerTurn = new JLabel();
		super.displayWinner = new JLabel();
		super.boardImage = new ImageIcon("棋盤.jpg");
		pieceList = new ArrayList<TaiwanChess>();
		this.displayPlayerHoldChess = new JLabel();
		
		super.Xsize = 8;
		super.Ysize = 4;
		chessOnBoard = new int[Xsize][Ysize];
		board.setLayout(new GridLayout(Ysize,Xsize));
		board.setIcon(boardImage);
	}
	

	public JLabel getDisplayPlayerHoldChess() {
		return displayPlayerHoldChess;
	}


	public void setDisplayPlayerHoldChess(JLabel displayPlayerHoldChess) {
		displayPlayerHoldChess = displayPlayerHoldChess;
	}


	@Override
	public void newBoard() {
		
		final int COMMANDER_NUM = 1;
		final int KNIGHT_NUM = 2;
		final int DEPUTY_NUM = 2;
		final int CHARIOT_NUM = 2;
		final int HORSE_NUM = 2;
		final int GUN_NUM = 2;
		final int SOLDIER_NUM = 5;
		final int TOTAL_NUM = 16;
		int i = 0;
		int X ,Y ,chessNum = 0,rank = 0;
		String color;
		Random random = new Random();
		
		newChess();
		
		color = "Black";
		X = random.nextInt(Xsize);
		Y = random.nextInt(Ysize);
		
		while(i < TOTAL_NUM){
			
			while(chessOnBoard[X][Y] != 0){
				X = random.nextInt(Xsize);
				Y = random.nextInt(Ysize);
			}
			
			switch(i){
			
				case 0 :
					rank = COMMANDER;
					chessNum = COMMANDER_NUM;
					break;
				case COMMANDER_NUM : 
					rank = KNIGHT;
					chessNum = KNIGHT_NUM;
					break;
				case COMMANDER_NUM + KNIGHT_NUM : 
					rank = DEPUTY;
					chessNum = DEPUTY_NUM;
					break;
				case COMMANDER_NUM + KNIGHT_NUM + DEPUTY_NUM :
					rank = CHARIOT;
					chessNum = CHARIOT_NUM;
					break;
				case COMMANDER_NUM + KNIGHT_NUM + DEPUTY_NUM + CHARIOT_NUM :
					rank = HORSE;
					chessNum = HORSE_NUM;
					break;
				case COMMANDER_NUM + KNIGHT_NUM + DEPUTY_NUM + CHARIOT_NUM + HORSE_NUM :
					rank = GUN;
					chessNum = GUN_NUM;
					break;
				case COMMANDER_NUM + KNIGHT_NUM + DEPUTY_NUM + CHARIOT_NUM + HORSE_NUM + GUN_NUM :
					rank = SOLDIER;
					chessNum = SOLDIER_NUM;
					break;
				
			}
			setChess(X,Y,rank,chessNum,color);
			
			i = i + 1;
			if(i == TOTAL_NUM && color.equals("Black")){
				i = 0;
				color = "Red";
			}
		}
		
		
	}
	private void newChess(){
		
		int i = 0;
		
		while(i < Xsize * Ysize){
			TaiwanChess piece = new TaiwanChess();
			piece.addMouseListener(ruleOfMoving);
			board.add(piece);
			pieceList.add(piece);
			i = i + 1; 
		}
	}
	
	private void setChess(int X ,int Y ,int rank ,int chessNum ,String color){
		
		ImageIcon blackCommander = new ImageIcon("黑將.png");
		ImageIcon blackKnight = new ImageIcon("黑士.png");
		ImageIcon blackDeputy = new ImageIcon("黑象.png");
		ImageIcon blackChariot = new ImageIcon("黑車.png");
		ImageIcon blackHorse = new ImageIcon("黑馬.png");
		ImageIcon blackGun = new ImageIcon("黑炮.png");
		ImageIcon blackSoldier = new ImageIcon("黑卒.png");
		ImageIcon redCommander = new ImageIcon("紅帥.png");
		ImageIcon redKnight = new ImageIcon("紅士.png");
		ImageIcon redDeputy = new ImageIcon("紅象.png");
		ImageIcon redChariot = new ImageIcon("紅車.png");
		ImageIcon redHorse = new ImageIcon("紅馬.png");
		ImageIcon redGun = new ImageIcon("紅炮.png");
		ImageIcon redSoldier = new ImageIcon("紅兵.png");
		ImageIcon back = new ImageIcon("背面.png");
		
		TaiwanChess piece = pieceList.get((Y*Xsize) + X);

		if(color.equals("Black")){
			chessOnBoard[X][Y] = rank;

		}else{
			chessOnBoard[X][Y] = rank + 7;
			
		}
		switch(chessOnBoard[X][Y]){
		
			case B_SOLDIER :
				piece.setFrontSide(blackSoldier);
				break;
			case B_GUN :
				piece.setFrontSide(blackGun);
				break;
			case B_HORSE :
				piece.setFrontSide(blackHorse);
				break;
			case B_CHARIOT :
				piece.setFrontSide(blackChariot);
				break;
			case B_DEPUTY :
				piece.setFrontSide(blackDeputy);
				break;
			case B_KNIGHT :
				piece.setFrontSide(blackKnight);
				break;
			case B_COMMANDER :
				piece.setFrontSide(blackCommander);
				break;
			case R_SOLDIER :
				piece.setFrontSide(redSoldier);
				break;
			case R_GUN :
				piece.setFrontSide(redGun);
				break;
			case R_HORSE :
				piece.setFrontSide(redHorse);
				break;
			case R_CHARIOT :
				piece.setFrontSide(redChariot);
				break;
			case R_DEPUTY :
				piece.setFrontSide(redDeputy);
				break;
			case R_KNIGHT :
				piece.setFrontSide(redKnight);
				break;
			case R_COMMANDER :
				piece.setFrontSide(redCommander);
				break;
		}
		
		piece.setBackSide(back);
		piece.setXposition(X);
		piece.setYposition(Y);
		piece.setColor(color);
		piece.setRank(rank);
		//piece.openChess();
			
	}
	public void updateDisplayPlayerTurn(){
		
		TaiwanRuleOfMoving t = (TaiwanRuleOfMoving) this.ruleOfMoving;
		
		this.updateDisplayWinner();

		this.displayPlayerTurn.setText("輪到player" + t.getPlayerFlag() + " : " + t.getColorOfCurrentPlayer());

		this.displayPlayerTurn.setFont(new Font("標楷體", Font.BOLD, 20));

	}
	
	public void updateDisplayPlayerHoldChess(){
		
		TaiwanRuleOfMoving t = (TaiwanRuleOfMoving) this.ruleOfMoving;
		String TaiwanChessGather = "無卒包馬車象士將兵炮傌俥相仕帥";
		String displayPlayerHoldChess;
		int X,Y;
		if(t.getFirstClickChess() == null){
			displayPlayerHoldChess = "你正拿著: ";
		}else{
			X = t.getFirstClickChess().getXposition();
			Y = t.getFirstClickChess().getYposition();
			displayPlayerHoldChess = "你正拿著: " + TaiwanChessGather.charAt(chessOnBoard[X][Y]);
		}

		this.displayPlayerHoldChess.setText(displayPlayerHoldChess);
		this.displayPlayerHoldChess.setFont(new Font("標楷體", Font.BOLD, 20));
		
	}
	public void updateDisplayWinner(){
		TaiwanRuleOfVictory t = (TaiwanRuleOfVictory)this.ruleOfVictory;
		
		this.displayWinner.setText("勝利判定： " + t.adjudgeVictory());
		this.displayWinner.setFont(new Font("標楷體", Font.BOLD, 20));
		if(t.adjudgeVictory().equals("Black")|| t.adjudgeVictory().equals("Red")){
			this.terminateGame();
		}
	}
	public void terminateGame(){
		
		int i = 0;
		while(i < this.pieceList.size()){
			pieceList.get(i).removeMouseListener(ruleOfMoving);;
			i = i + 1;
		}
	}

	

}
