
public class GomokuRuleOfVictory extends RuleOfVictory{
	
	
	public GomokuRuleOfVictory(GomokuChessBoard b){
		
		super.board = b;
		super.chessOnBoard = b.getChessOnBoard();
		
	}
	
	public boolean adjudgeVictory(int X ,int Y ,int color){

		if(this.adjudgeLeftToRight(X, Y, color) == false &&
			this.adjudgeLeftupToRightDown(X, Y, color) == false &&
			this.adjudgeRightUpToLeftDown(X, Y, color) == false &&
			this.adjudgeUpToDown(X, Y, color) == false){
			return false;
		}else{
			return true;
		}
		
	}
	
	private boolean adjudgeLeftToRight(int X ,int Y ,int color){
		
		int leftX;
		int rightX;
		leftX = X;
		rightX = X;
		
		while(leftX-1 > 0 && chessOnBoard[leftX-1][Y] == color){
			leftX = leftX - 1;
		}
		
		while(rightX+1 < board.getXsize() && chessOnBoard[rightX+1][Y] == color){
			rightX = rightX + 1;
		}
		
			
		if(rightX - leftX >= 4){
			return true;
		}else{
			return false;
		}
		
	}
	
	private boolean adjudgeUpToDown(int X ,int Y ,int color){
		
		int upY;
		int downY;
		upY = Y;
		downY = Y;
		
		while(upY-1 > 0 && chessOnBoard[X][upY-1] == color){
			upY = upY - 1;
		}
		while(downY+1 < board.getYsize() && chessOnBoard[X][downY+1] == color){
			downY = downY + 1;
		}
		
		if(downY - upY >= 4){
			return true;
		}else{
			return false;
		}
	}
	
	private boolean adjudgeLeftupToRightDown(int X ,int Y ,int color){
		int upY;
		int downY;
		int leftX;
		int rightX;
		
		upY = Y;
		downY = Y;
		leftX = X;
		rightX = X;
		
		while(upY-1 > 0 && leftX-1 > 0 && chessOnBoard[leftX-1][upY-1] == color){
			upY = upY - 1;
			leftX = leftX - 1;
		}
		while(downY+1 < board.getYsize() && rightX+1 < board.getXsize() && chessOnBoard[rightX+1][downY+1] == color){
			downY = downY + 1;
			rightX  = rightX + 1;
		}
		
		if(downY - upY >= 4){
			return true;
		}else{
			return false;
		}
		
	}
	
	private boolean adjudgeRightUpToLeftDown(int X ,int Y ,int color){
		int upY;
		int downY;
		int leftX;
		int rightX;
		
		upY = Y;
		downY = Y;
		leftX = X;
		rightX = X;
		
		while(upY-1 > 0 && rightX+1 < board.getXsize() && chessOnBoard[rightX+1][upY-1] == color){
			upY = upY - 1;
			rightX  = rightX + 1;
		}
		while(downY+1 < board.getYsize() && leftX-1 > 0 && chessOnBoard[leftX-1][downY+1] == color){
			downY = downY + 1;
			leftX = leftX - 1;
		}
		
		if(downY - upY >= 4){
			return true;
		}else{
			return false;
		}
	}


}
