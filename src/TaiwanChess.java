import javax.swing.ImageIcon;


public class TaiwanChess extends Chess{
	
	private ImageIcon frontSide;
	private ImageIcon backSide;
	private boolean existenceFlag;
	private boolean openFlag;
	private int rank;
	
	public TaiwanChess(){
		existenceFlag = true;
		openFlag = false;
	}
	
	public TaiwanChess(int x ,int y ,int r ,String c){
		Xposition = x;
		Yposition = y;
		rank =  r;
		color = c;
	}


	public ImageIcon getFrontSide() {
		return frontSide;
	}


	public void setFrontSide(ImageIcon frontSide) {
		this.frontSide = frontSide;
	}


	public ImageIcon getBackSide() {
		return backSide;
	}


	public void setBackSide(ImageIcon backSide) {
		this.backSide = backSide;
		this.setIcon(backSide);
	}


	public boolean isExistenceFlag() {
		return existenceFlag;
	}


	public void setExistenceFlag(boolean existenceFlag) {
		this.existenceFlag = existenceFlag;
	}
	

	public boolean isOpenFlag() {
		return openFlag;
	}

	public void setOpenFlag(boolean openFlag) {
		this.openFlag = openFlag;
	}

	public int getRank() {
		return rank;
	}


	public void setRank(int rank) {
		this.rank = rank;
	}
	
	public void openChess(){
		this.setIcon(frontSide);
		this.openFlag = true;
	}
	public void removeChess(){
		this.existenceFlag = false;
		this.setIcon(null);
	}

}
