
public abstract class ChessGameEntity {

	
	public abstract ChessBoard createChessBoard();
	
	public abstract RuleOfMoving createRuleOfMoving();
	
	public abstract RuleOfVictory createRuleOfVictory();
	
	public abstract Chess createChess();
	
	public abstract Screen createScreen();
	
	
}
