import java.awt.Image;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;


public abstract class ChessBoard {
	
	protected int[][] chessOnBoard;
	protected RuleOfMoving ruleOfMoving;
	protected RuleOfVictory ruleOfVictory;
	protected JLabel board;	
	protected JLabel displayPlayerTurn;
	protected JLabel displayWinner;
	protected ImageIcon boardImage;
	protected int Xsize;
	protected int Ysize;
	
	
	

	public int[][] getChessOnBoard() {
		return chessOnBoard;
	}

	public JLabel getDisplayPlayerTurn() {
		return displayPlayerTurn;
	}

	public JLabel getDisplayWinner() {
		return displayWinner;
	}

	public RuleOfMoving getRuleOfMoving() {
		return ruleOfMoving;
	}

	public void setRuleOfMoving(RuleOfMoving ruleOfMoving) {
		this.ruleOfMoving = ruleOfMoving;
	}

	public RuleOfVictory getRuleOfVictory() {
		return ruleOfVictory;
	}

	public void setRuleOfVictory(RuleOfVictory ruleOfVictory) {
		this.ruleOfVictory = ruleOfVictory;
	}

	public JLabel getBoard(){
		return board;
	}

	public ImageIcon getBoardImage() {
		return boardImage;
	}

	public void setBoardImage(ImageIcon boardImage) {
		this.boardImage = boardImage;
	}

	public int getXsize() {
		return Xsize;
	}

	public int getYsize() {
		return Ysize;
	}
	
	public abstract void newBoard();
	
	public abstract void terminateGame();


}
