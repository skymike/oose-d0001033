
public class TaiwanRuleOfVictory extends RuleOfVictory{

	public TaiwanRuleOfVictory(TaiwanChessBoard b) {
		
		super.board = b;
		chessOnBoard = b.getChessOnBoard();
		
		
	}

	public String adjudgeVictory(){
		int x = 0,y = 0;
		boolean blackTerminateGameFlag = true,redTerminateGameFlag = true;
		
		while(y < board.getYsize()){
			x = 0;
			while(x < board.getXsize()){
				if(chessOnBoard[x][y] > 8){
					blackTerminateGameFlag = false;
				}
				x = x + 1;
			}
			y = y + 1;
		}
		x = 0;
		y = 0;
		while(y < board.getYsize()){
			x = 0;
			while(x < board.getXsize()){
				if(chessOnBoard[x][y] < 8){
					redTerminateGameFlag = false;
				}
				x = x + 1;
			}
			y = y + 1;
		}
		if(blackTerminateGameFlag == true){
			return "Black";
		}else if(redTerminateGameFlag == true){
			return "Red";
		}else{
			return "�i�椤...";
		}
	}
	
}
